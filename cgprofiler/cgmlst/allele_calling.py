"""
Allele calling in two steps:
1. detection of exact matches ("Autotag")
2. detection of new alleles on loci for which no exact match was found
    ("Scannew")
"""

import pandas as pd
import pickle

from collections import defaultdict
from contextlib import nullcontext

from pathlib import Path
from tqdm import tqdm

from cgprofiler.cgmlst import (
    autotag,
    blast_functions,
    data_utils,
    file_utils,
    scannew,
)
from cgprofiler import utility


def allele_calling(args):
    """
    Main cgMLST function. Find exact allele matches and detect new ones.

    Write a tsv, one line per assembly, each locus a column.
    """

    with (
        utility.timeit("Total running time") if args.verbose else nullcontext()
    ):
        verbose = args.verbose

        loci_names = file_utils.extract_loci_names(args.scheme_dir)

        # Create a BLAST database if not provided
        if not args.blast_db_path:
            blast_db_path = Path("tmp_blast_db/")
            blast_functions.create_blast_db(
                scheme_path=args.scheme_dir,
                blast_db_name="tmp_blast_db",
                blast_db_path=blast_db_path,
                verbose=verbose,
            )
            blast_db_path = blast_db_path / "tmp_blast_db.fasta"
        else:
            blast_db_path = args.blast_db_path

        # Initiate output dataframe
        output_df = pd.DataFrame(columns=["seq_id"] + sorted(loci_names))

        strains_w_tmp_alleles = {}

        # Process one sequence at the time.
        for query in tqdm(args.query):
            if verbose:
                print(f"\n\nProcessing {str(query)}\n")

            # Autotag: get exact matches.
            with (
                utility.timeit("Autotag running time")
                if args.verbose
                else nullcontext()
            ):
                autotag_dict = autotag.autotag(
                    query=query,
                    blast_db_path=blast_db_path,
                    num_threads=args.num_threads,
                    autotag_word_size=args.autotag_word_size,
                    verbose=verbose,
                )

            # Extract loci for which no exact match was found
            # during autotag
            loci_no_exact_match = [
                locus
                for locus in loci_names
                if locus not in autotag_dict.keys()
            ]

            # Skip scannew phase if empty
            if not loci_no_exact_match:
                continue

            # Scannew: get new and incomplete alleles.
            with (
                utility.timeit("Scannew running time")
                if args.verbose
                else nullcontext()
            ):
                new_alleles, scannew_dict = scannew.scannew(
                    query,
                    loci_no_exact_match,
                    scheme_dir=args.scheme_dir,
                    verbose=verbose,
                    num_threads=args.num_threads,
                    cds=args.cds,
                    detailed=args.detailed,
                    min_id=args.min_id_new_allele,
                    min_cov_new_allele=args.min_cov_new_allele,
                    min_cov_incomplete=args.min_cov_incomplete,
                )

            allele_calling_dict = {
                "seq_id": query,
                **autotag_dict,
                **scannew_dict,
            }

            # +1 for "seqid"
            assert (
                len(allele_calling_dict) == len(loci_names) + 1
            ), "Not all loci are present in the output dictionary"

            if not new_alleles.empty:
                allele_calling_dict.update(
                    **new_alleles.set_index("locus")["md5"].to_dict()
                )
                strains_w_tmp_alleles[str(query)] = list(new_alleles["locus"])

            output_df = pd.concat(
                [output_df, pd.DataFrame([allele_calling_dict])],
                ignore_index=True,
            )

        output_df["seq_id"] = output_df["seq_id"].apply(lambda x: str(x))
        output_df.set_index("seq_id", inplace=True)

        """
        Write profiles with temprorary alleles in a Python pickle
        (platform use only)
        TODO: change pickle to nested json
        """
        if strains_w_tmp_alleles and args.profiles_w_tmp_alleles:
            args.profiles_w_tmp_alleles.parents[0].mkdir(
                parents=True, exist_ok=True
            )
            # Load existing profiles if the file exists,
            # otherwise initialize an empty dictionary
            profiles_dict = (
                pickle.load(open(args.profiles_w_tmp_alleles, "rb"))
                if args.profiles_w_tmp_alleles.exists()
                else {}
            )

            for strain in strains_w_tmp_alleles.keys():
                profiles_dict[strain] = {
                    "genome": strain,
                    "profile": str(args.out),
                    "tmp_loci": strains_w_tmp_alleles[strain],
                }

            with open(args.profiles_w_tmp_alleles, "wb+") as f:
                pickle.dump(profiles_dict, f)

        if strains_w_tmp_alleles:
            # Convert {profile : [loci with tmp allele]}
            # to {locus: [profiles with tmp allele for that locus]}.
            locus_tmp_alleles = defaultdict(list)
            for profile, loci in strains_w_tmp_alleles.items():
                for locus in loci:
                    locus_tmp_alleles[locus].append(profile)

            """Provide temporary allele numbers instead of hash
            """
            if not args.hash:
                # If number of alleles per locus provided in a file, parse it.
                if args.num_alleles_per_locus:
                    with open(args.num_alleles_per_locus) as f:
                        num_official_alleles = dict(
                            line.strip().split("\t") for line in f
                        )
                else:
                    num_official_alleles = file_utils.get_num_alleles(
                        args.scheme_dir
                    )

                output_df = data_utils.hash_to_numerical(
                    output_df, locus_tmp_alleles, num_official_alleles
                )

        args.out.parents[0].mkdir(parents=True, exist_ok=True)
        with open(args.out, "w+") as f:
            output_df.to_csv(f, sep="\t", index=True)

        if verbose:
            print("\nFinished.")


if __name__ == "__main__":
    allele_calling()
