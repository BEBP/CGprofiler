"""
CDS option

In scannew, extract new alleles' coding sequences
only work on a cgMLST scheme where alleles are defined as CDS
"""

import pandas as pd
import tempfile
import warnings
from pathlib import Path
from Bio.Seq import Seq

from cgprofiler.cgmlst.file_utils import (
    get_sequence_from_fasta_file,
    get_locus_file,
)
from cgprofiler.cgmlst.blast_functions import run_blast
from cgprofiler.cgmlst.blast_config import BlastConfig

TBLASTN_OUTFMT = (
    "6",
    "qseqid",
    "sseqid",
    "bitscore",
    "length",
    "pident",
    "sstart",
    "send",
    "sframe",
)

# M, V, L
START_CODONS = ("ATG", "GTG", "TTG")
STOP_CODONS = ("TAA", "TAG", "TGA")


def is_cds(seq: str) -> bool:
    """Wether a DNA sequence is a coding sequence.

    Check if sequence length is a multiple of 3 and start/stop codons.
    Do not check if nested stop codon.
    """
    if not (
        len(seq) % 3 == 0
        and (seq[:3] in START_CODONS and seq[-3:] in STOP_CODONS)
    ):
        return False
    return True


def has_nested_stop_codon(seq: str) -> bool:
    """Wether a DNA sequence contains a stop codon, reading frame +1."""
    for i in range(3, len(seq) - 6 + 1, 3):
        if seq[i : i + 3] in STOP_CODONS:
            return True
    return False


def trim_cds(contig: str, pstart: int, pend: int) -> str:
    """Trim a DNA sequence on CDS. Search for a START and STOP
    codon.

    :param seq: DNA sequence to trim
    :type seq: str
    :return: DNA sequence starting with start/stop codon, length %3
        no stop codon within.
    :rtype: str
    """
    if pstart > pend:
        contig = str(Seq(contig).reverse_complement())
        pstart, pend = pend, pstart

    if abs(pend + 1 - pstart) % 3 != 0:
        warnings.warn("Length provided not %3.", UserWarning)
        return ""

    # Half of the contig length converted in codons
    search_range = (pend - pstart) // (3 * 0.5)

    # Search for start codon
    # initiative actual start at putative start
    astart = pstart - 1
    while astart >= 0 and pstart - astart <= search_range:
        if contig[astart : astart + 3] in START_CODONS:
            break
        astart -= 3
    if astart < 0 or pstart - astart > search_range:
        # No start codon found within the search range, return a warning
        warnings.warn(
            "No start codon found within the search range.", UserWarning
        )
        return ""

    # Search for stop codon
    aend = pend
    while aend <= len(contig) and aend - pend <= search_range:
        if contig[aend - 3 : aend] in STOP_CODONS:
            break
        aend += 3
    if aend > len(contig) or aend - pend > search_range:
        warnings.warn(
            "No stop codon found within the search range.", UserWarning
        )
        return ""
    return contig[astart:aend]


def get_cds(
    new_alleles: pd.DataFrame,
    query: Path,
    scheme_dir: Path,
    num_threads: int,
    verbose: bool
) -> pd.DataFrame:
    """Extract the coding sequence (CDS).

    Only work on a scheme where alleles are already CDS.

    Run a tBLASTn on detected new alleles (query: allele database translated,
    subject: genome).
    Look for a START and STOP codon; if not found return an empty sequence.

    Modifiy the input dataframe.

    :param new_alleles: new alleles detected by scannew
    :type new_alleles: pd.DataFrame
    :param query: The query sequence in FASTA format.
    :type query: pathlib.Path
    :param scheme_dir: The directory containing the scheme files.
    :type scheme_dir: pathlib.Path
    :param num_threads: Number of threads to run tBLASTn on.
    :type num_threads: int
    :param verbose: Verbosity.
    :type verbose: bool
    :return: new alleles df with CDS sequences, empty sequences otherwise
    :rtype: pd.DataFrame
    """
    if verbose:
        print("--- Enter CDS mode ---")

    # See whether some alleles are already CDS
    new_alleles["is_cds"] = new_alleles.apply(
        lambda row: is_cds(row["qseq"]), axis=1
    )
    new_alleles_already_cds = new_alleles.loc[new_alleles["is_cds"]]
    new_alleles_already_cds = new_alleles_already_cds.rename(
        columns={"qseq": "cds"}
    )
    new_alleles_not_cds = new_alleles[~new_alleles["is_cds"]]

    if new_alleles_not_cds.empty:
        new_alleles_cds = new_alleles_already_cds

    else:
        """Try to extract the CDS from a tBLASTn
        """
        # Extract the entire sequences of the alleles from the scheme files
        new_alleles_complete_seq = \
            new_alleles_not_cds[["locus", "allele"]].copy()

        new_alleles_complete_seq["allele_seq_dna"] = (
            new_alleles_complete_seq.apply(
                lambda row: get_sequence_from_fasta_file(
                    filename=get_locus_file(
                        locus=row["locus"], scheme_path=scheme_dir
                    ),
                    contig_id=row["locus"] + "_" + row["allele"],
                ),
                axis=1,
            )
        )

        # Translate the allele using BioPython.
        new_alleles_complete_seq["allele_seq_prot"] = new_alleles_complete_seq[
            "allele_seq_dna"
        ].apply(lambda dna_seq: str(Seq(dna_seq).translate(table=11)))

        # Merge into a single file and run tBLASTn.
        with tempfile.TemporaryDirectory() as tmp_dir:
            query_file = Path(tmp_dir) / "seq_prot_joined.fasta"
            with open(query_file, "w+") as tmp_f:
                for _, row in new_alleles_complete_seq.iterrows():
                    tmp_f.write(f">{row['locus']}\n{row['allele_seq_prot']}\n")
            with open(query_file, "r"):
                tblastn_config = BlastConfig(
                    query_path=Path(tmp_f.name),
                    blast_type="tblastn",
                    subject=query,
                    num_threads=num_threads,
                    evalue="1E-20",
                    ungapped=False,
                    outfmt=TBLASTN_OUTFMT,
                    task="tblastn",
                    word_size=31,
                )
                tblastn_df = run_blast(tblastn_config)

        # Keep hit with highest bitscore for each query (allele)
        tblastn_df = tblastn_df.loc[
            tblastn_df.groupby("qseqid")["bitscore"].idxmax()
        ]
        tblastn_df.rename(columns={"qseqid": "locus"}, inplace=True)

        # Extract the entire contig sequences (subject)
        new_alleles_search_cds = tblastn_df[
            ["locus", "sseqid", "sstart", "send", "sframe"]
        ].copy()

        new_alleles_search_cds["contig_seq"] = new_alleles_search_cds.apply(
            lambda row: get_sequence_from_fasta_file(
                filename=query,
                contig_id=row["sseqid"],
            ),
            axis=1,
        )

        # Trim on alignment start and stop position
        new_alleles_search_cds[["cds"]] = new_alleles_search_cds.apply(
            lambda row: trim_cds(row["contig_seq"],
                                 row["sstart"],
                                 row["send"]),
            axis=1,
        ).apply(pd.Series)

        new_alleles_cds = pd.concat(
            [
                new_alleles_already_cds[["locus", "cds"]],
                new_alleles_search_cds[["locus", "cds"]],
            ],
            ignore_index=True,
        )

    # Whether contains nested stop codon
    new_alleles_cds[["nested_stop"]] = new_alleles_cds.apply(
        lambda row: has_nested_stop_codon(str(row["cds"])),
        axis=1,
    ).apply(pd.Series)

    return new_alleles_cds
