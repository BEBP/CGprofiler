import json

from pathlib import Path

from cgprofiler.db.api_bigsdb import (
    download_scheme as download_scheme_bigsdb,
)

from cgprofiler.db.request_schemes_enterobase import (
    download_scheme as download_scheme_enterobase,
)


def download_scheme(scheme_file: Path, species: str, output_dir: Path):
    """
    Download the scheme for the given species from either BIGsdb or Enterobase.

    This function selects the appropriate method to download the scheme based
    on the species provided. If the species is found in the BIGsdb database,
    it uses the download method specific to BIGsdb. Otherwise, if the species
    is found in the Enterobase database, it uses the download method specific
    to Enterobase.

    :param scheme_file: The path to the scheme file (scheme_api.json).
    :param species: The name of the species.
    :param output_dir: The directory where to download the scheme files.
    :raises ValueError: If no scheme is found for the given species.
    """
    with open(scheme_file, "r") as file:
        schemes = json.load(file)

    if species == "av":
        print(list(schemes.keys()))

    elif species in schemes.keys():
        scheme_info = schemes.get(species)
        scheme_url = scheme_info.get("url")
        scheme_platform = scheme_info.get("platform")
        if scheme_platform == "BIGSdb":
            download_scheme_bigsdb(scheme_url, output_dir)
        elif scheme_platform == "Enterobase":
            download_scheme_enterobase(scheme_url, output_dir)
        else:
            raise ValueError(
                f"Unknown platform '{scheme_platform}'"
                f"for species '{species}'"
            )
    else:
        raise ValueError(f"No scheme found for species '{species}'")
