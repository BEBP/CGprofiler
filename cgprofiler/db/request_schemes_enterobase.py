import gzip
import io
import requests

from bs4 import BeautifulSoup
from typing import Dict
import re


def get_last_update_locus(scheme_html_parser: BeautifulSoup) -> Dict[str, str]:
    """
    Extracts the last update date of each locus from the scheme HTML parser.

    This function parses the HTML content of a scheme page and extracts the
    last update date of each locus. It specifically looks for links to
    .fasta.gz files and retrieves the corresponding date.

    :param scheme_html_parser: The BeautifulSoup HTML parser containing
        the scheme page content.
    :return: A dictionary mapping locus names to their last update dates.
    :rtype: Dict[str, str]
    """
    date_pattern = re.compile(r"\d{2}-[a-zA-Z]{3}-\d{4}")

    href_date_map = {}

    for link in scheme_html_parser.find_all("a"):
        href = link.get("href")
        text = link.next_sibling.strip()
        date_match = date_pattern.search(text)
        if href and date_match:
            if href.endswith(".fasta.gz"):
                href = href.replace(".fasta.gz", "")
                href_date_map[href] = date_match.group()
    return href_date_map


def download_locus_file(locus_url: str, download_path: str):
    """
    Download a locus file from the given URL and decompress it.

    :param locus_url: The URL of the locus file.
    :param download_path: The path to save the downloaded file.
    :raises RuntimeError: If the locus file could not be downloaded or
        decompressed.
    """
    response = requests.get(locus_url)
    if response.ok:
        try:
            with gzip.GzipFile(fileobj=io.BytesIO(response.content)) as f_in:
                with open(download_path, "wb") as f_out:
                    f_out.write(f_in.read())
            print(
                f"File '{download_path}' downloaded and decompressed"
                "successfully."
            )
        except Exception as e:
            raise RuntimeError(
                f"Failed to decompress file '{locus_url}'. Error: {e}"
            )
    else:
        raise RuntimeError(
            f"Failed to download file '{locus_url}'."
            f"Status code: {response.status_code}"
        )


def download_scheme(scheme_url: str, download_dir: str):
    """
    Download all loci files for the given species from Enterobase.

    :param species: The name of the species.
    :param url_scheme_file: The path to the JSON file containing the URLs.
    :param download_dir: The directory where to download the files.
    :raises RuntimeError: If any locus file could not be downloaded or
        decompressed.
    """
    response = requests.get(scheme_url)
    if response.ok:
        scheme_html_parser = BeautifulSoup(response.content, "html.parser")
        loci_dates = get_last_update_locus(scheme_html_parser)

        for locus in loci_dates:
            locus_url = f"{scheme_url}/{locus}.fasta.gz"
            download_path = f"{download_dir}/{locus}.fasta"
            download_locus_file(locus_url, download_path)
    else:
        raise RuntimeError(
            f"Failed to fetch scheme HTML from URL '{scheme_url}'. "
            f"Status code: {response.status_code}"
        )


# TODO
def update_scheme():
    pass
