from pathlib import Path
from tempfile import TemporaryDirectory
import pytest

from cgprofiler.cgmlst import blast_functions


@pytest.fixture(scope="package")
def kp_genome():
    return Path("tests/data/kp/genomes/10_04A025_hennart2022.fas")


@pytest.fixture(scope="package")
def ecoli_genome():
    return Path("tests/data/ecoli/genomes/ESC_AC5431AA_AS.fasta")


@pytest.fixture(scope="package")
def kp_db_sample():
    """Klebsiella peunmoniae BLASTdb"""
    scheme_path = Path("tests/data/kp/scheme_sample/")
    with TemporaryDirectory() as db_dir:
        db_path = Path(db_dir)
        blast_functions.create_blast_db(
            scheme_path=scheme_path,
            blast_db_name="kp_db_sample",
            blast_db_path=db_path,
            verbose=True,
        )
        yield db_path / "kp_db_sample.fasta"


@pytest.fixture(scope="package")
def ecoli_db_sample():
    """Escherichia coli BLASTdb"""
    scheme_path = Path("tests/data/ecoli/scheme_sample/")
    with TemporaryDirectory() as db_dir:
        db_path = Path(db_dir)
        blast_functions.create_blast_db(
            scheme_path=scheme_path,
            blast_db_name="ecoli_db_sample",
            blast_db_path=db_path,
            verbose=True,
        )
        yield db_path / "ecoli_db_sample.fasta"
