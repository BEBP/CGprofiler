import hashlib
import pickle
import pytest
from argparse import Namespace
from pathlib import Path

from cgprofiler.cgmlst import allele_calling


@pytest.fixture(scope="module")
def args_kp(kp_genome):
    args_kp = Namespace(
        query=[kp_genome],
        scheme_dir=Path("tests/data/kp/scheme_sample"),
        verbose=False,
        blast_db_path=Path(
            "tests/data/kp/scheme_sample_blastdb/scheme_sample_blastdb.fasta"
        ),
        num_threads=4,
        autotag_word_size=31,
        cds=False,
        detailed=False,
        min_id_new_allele=90,
        min_cov_new_allele=70,
        min_cov_incomplete=70,
        profiles_w_tmp_alleles="",
        hash=False,
        num_alleles_per_locus="",
    )
    return args_kp


######################
# --- user runs ---- #
######################


def test_allele_calling_minimal_run(args_kp, tmp_path):
    """
    Minimal run, no database provided, only user profiles put out
    """
    args_kp.out = tmp_path / "test_output.tsv"
    args_kp.blast_db_path = ""

    allele_calling.allele_calling(args_kp)
    assert args_kp.out.exists()

    with open(args_kp.out, "r") as file:
        content = file.read()
        expected_data = (
            "seq_id	accB_S\taccC_S\taccD_S\n"
            + "tests/data/kp/genomes/10_04A025_hennart2022.fas\t1\t2\t2\n"
        )
        assert content == expected_data


def test_allele_calling_minimal_with_db(args_kp, tmp_path):
    """
    Database provided, only user profiles put out
    """
    args_kp.out = tmp_path / "test_output.tsv"

    allele_calling.allele_calling(args_kp)
    assert args_kp.out.exists()

    with open(args_kp.out, "r") as file:
        content = file.read()
        expected_data = (
            "seq_id	accB_S\taccC_S\taccD_S\n"
            + "tests/data/kp/genomes/10_04A025_hennart2022.fas\t1\t2\t2\n"
        )
        assert content == expected_data


##########################
# --- platform runs ---- #
##########################


def test_allele_calling_platform_output(args_kp, tmp_path):
    """
    Database provided, put out profile_w_tmp_alleles.pickle
    """
    args_kp.out = tmp_path / "test_output.tsv"
    args_kp.profiles_w_tmp_alleles = (
        tmp_path / "profiles_w_tmp_alleles_file.pickle"
    )

    allele_calling.allele_calling(args_kp)

    assert args_kp.out.exists()
    with open(args_kp.out, "r") as file:
        content = file.read()
    expected_data = (
        "seq_id	accB_S\taccC_S\taccD_S\n"
        + "tests/data/kp/genomes/10_04A025_hennart2022.fas\t1\t2\t2\n"
    )
    assert content == expected_data

    assert args_kp.profiles_w_tmp_alleles.exists()
    with open(args_kp.profiles_w_tmp_alleles, "rb") as f:
        output_dict = pickle.load(f)
    assert output_dict == {
        "tests/data/kp/genomes/10_04A025_hennart2022.fas": {
            "genome": "tests/data/kp/genomes/10_04A025_hennart2022.fas",
            "profile": str(args_kp.out),
            "tmp_loci": ["accC_S", "accD_S"],
        }
    }


def test_allele_calling_platform_hash(args_kp, tmp_path):
    """
    Verify that MD5-hash are correct
    """
    args_kp.out = tmp_path / "test_output.tsv"
    args_kp.hash = True

    with open(Path("tests/data/test_scannew/kp_accC_S_20.tfa"), "r") as f:
        f.readline()
        accc_s_20 = f.readline().strip()
        hash_acc_s_20 = (
            "accC_S_" + hashlib.md5(str(accc_s_20).encode()).hexdigest()
        )

    with open(Path("tests/data/test_scannew/kp_accD_S_37.tfa"), "r") as f:
        f.readline()
        # minus sense
        accd_s_37 = f.readline().strip()
        hash_acc_d_37 = (
            "accD_S_" + hashlib.md5(str(accd_s_37).encode()).hexdigest()
        )

    allele_calling.allele_calling(args_kp)

    assert args_kp.out.exists()
    with open(args_kp.out, "r") as file:
        content = file.read()
    expected_data = (
        "seq_id	accB_S\taccC_S\taccD_S\n"
        + "tests/data/kp/genomes/10_04A025_hennart2022.fas\t"
        + f"1\t{hash_acc_s_20}\t{hash_acc_d_37}\n"
    )
    assert content == expected_data


def test_allele_calling_platform_num_alleles(args_kp, tmp_path):
    """
    Test num_alleles_per_locus
    """
    args_kp.out = tmp_path / "test_output.tsv"
    args_kp.num_alleles_per_locus = Path(
        "tests/data/num_alleles_per_locus/kp.tsv"
    )
    args_kp.hash = False

    allele_calling.allele_calling(args_kp)

    assert args_kp.out.exists()
    with open(args_kp.out, "r") as file:
        content = file.read()
    expected_data = (
        "seq_id	accB_S\taccC_S\taccD_S\n"
        + "tests/data/kp/genomes/10_04A025_hennart2022.fas\t"
        + "1\t19\t113\n"
    )
    assert content == expected_data
