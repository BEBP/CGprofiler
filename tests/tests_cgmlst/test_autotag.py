from cgprofiler.cgmlst import autotag

num_threads = 4


def test_autotag_kp(kp_genome, kp_db_sample):
    expected_matches = {
        "accB_S": "1",
    }
    matches = autotag.autotag(
        query=kp_genome,
        blast_db_path=kp_db_sample,
        autotag_word_size=31,
        num_threads=num_threads,
        verbose=False,
    )
    assert matches == expected_matches


def test_autotag_ecoli(ecoli_genome, ecoli_db_sample):
    expected_matches = {
        "b0894": "1",
        "b3437": "1",
    }
    matches = autotag.autotag(
        query=ecoli_genome,
        blast_db_path=ecoli_db_sample,
        autotag_word_size=31,
        num_threads=num_threads,
        verbose=False,
    )
    assert matches == expected_matches
