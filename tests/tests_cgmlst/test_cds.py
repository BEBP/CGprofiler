import pandas as pd
import pytest

from pathlib import Path

from cgprofiler.cgmlst.cds import (
    is_cds,
    get_cds,
    has_nested_stop_codon,
    trim_cds,
)


@pytest.mark.parametrize(
    "seq, expected",
    [
        # Valid
        ("ATGAAATAA", True),
        ("ATGAAATAG", True),
        ("ATGAAATGA", True),
        ("GTGAAATAA", True),
        ("GTGAAATAG", True),
        ("GTGAAATGA", True),
        ("TTGAAATAA", True),
        ("TTGAAATAG", True),
        ("TTGAAATGA", True),
        # Invalid sequences
        ("GTAAAATAA", False),  # No start codon
        ("ATGAAAAAA", False),  # No stop codon
        ("ATGAGTG", False),  # Length not %3
        ("", False),  # Empty
    ],
)
def test_is_cds(seq, expected):
    assert is_cds(seq) == expected


#################################
# --- has_nested_stop_codon --- #
#################################


@pytest.mark.parametrize(
    "seq, expected",
    [
        # Contains stop codon
        ("ATGTAATAA", True),
        ("ATGTAGTAG", True),
        ("ATGTGATGA", True),
        # No stop codon
        ("ATGAAATAA", False),
        ("GC" * 12, False),
        ("", False),
        ("ATGTAA", False),
    ],
)
def test_has_nested_stop_codon(seq, expected):
    assert has_nested_stop_codon(seq) == expected


##########################
# --- trim_cds tests --- #
##########################


@pytest.mark.parametrize(
    "contig, pstart, pend",
    [
        # Putative positions not %3
        ("ATGAAATAG", 1, 8),
        ("CTATTTCAT", 8, 1),
    ],
)
def test_trim_cds_wrong_putative_length(contig, pstart, pend):
    print(abs(pend + 1 - pstart) % 3 != 0)
    with pytest.warns(
        UserWarning,
        match="Length provided not %3.",
    ):
        trimmed = trim_cds(contig, pstart, pend)
        assert trimmed == ""


# Start and stop codon present
@pytest.mark.parametrize(
    "seq, pstart, pend, expected",
    [
        # search on the outside from start and stop positions
        # + strand
        ("ATGAAATAG", 1, 9, "ATGAAATAG"),
        ("ATGAAATTTGGGTAG", 4, 15, "ATGAAATTTGGGTAG"),
        ("ATGAAATTTGGGTAG", 4, 12, "ATGAAATTTGGGTAG"),
        ("GTGAAATAA", 1, 9, "GTGAAATAA"),
        ("GTGAAATTTGGGTGA", 4, 15, "GTGAAATTTGGGTGA"),
        ("TTGAAATTTGGGTAA", 4, 12, "TTGAAATTTGGGTAA"),
        # - strand
        ("CTATTTCAT", 9, 1, "ATGAAATAG"),
        ("CTACCCAAATTTCAT", 15, 4, "ATGAAATTTGGGTAG"),
        ("CTACCCAAATTTCAT", 12, 4, "ATGAAATTTGGGTAG"),
        ("TTATTTCAC", 9, 1, "GTGAAATAA"),
        ("TCACCCAAATTTCAC", 15, 4, "GTGAAATTTGGGTGA"),
        ("TTACCCAAATTTCAA", 12, 4, "TTGAAATTTGGGTAA"),
    ],
)
def test_trim_cds(seq, pstart, pend, expected):
    result = trim_cds(seq, pstart, pend)
    assert result == expected


#########################
# --- get_cds tests --- #
#########################


def test_get_cds_already_cds(kp_genome):
    query = kp_genome
    scheme_dir = Path("tests/data/kp/scheme_sample")

    # Mock new alleles input dataframe
    with open(Path("tests/data/test_scannew/kp_accC_S_20.tfa"), "r") as f:
        f.readline()
        accc_s_20 = f.readline().strip()

    with open(Path("tests/data/test_scannew/kp_accD_S_37.tfa"), "r") as f:
        f.readline()
        accd_s_37 = f.readline().strip()
    new_alleles = pd.DataFrame(
        [["accC_S", accc_s_20], ["accD_S", accd_s_37]],
        columns=["locus", "qseq"],
    )
    new_alleles_cds = get_cds(new_alleles, query, scheme_dir, 1, False)

    new_alleles.rename(columns={"qseq": "cds"}, inplace=True)
    print(new_alleles_cds)
    assert new_alleles[["locus", "cds"]].equals(
        new_alleles_cds[["locus", "cds"]]
    )
