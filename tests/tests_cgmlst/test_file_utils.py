from pathlib import Path

import pytest

from cgprofiler.cgmlst.file_utils import (
    ContigNotFound,
    extract_loci_names,
    get_locus_file,
    get_num_alleles,
    get_sequence_from_fasta_file,
)

####################################
# --- extract_loci_names tests --- #
####################################


def test_extract_loci_names():
    loci_names = extract_loci_names(Path("tests/data/kp/scheme_sample"))
    assert isinstance(loci_names, list)
    assert len(loci_names) == 3
    assert "accB_S" in set(loci_names)


@pytest.mark.exception
def test_extract_loci_names_invalid_directory():
    with pytest.raises(FileNotFoundError):
        extract_loci_names(Path("invalid_directory"))


@pytest.mark.exception
def test_extract_loci_names_no_loci(tmp_path):
    with pytest.raises(FileNotFoundError):
        extract_loci_names(tmp_path)


################################
# --- get_locus_file tests --- #
################################


@pytest.fixture
def scheme_path():
    return Path("tests/data/kp/scheme_sample")


@pytest.mark.parametrize(
    "locus, file_path",
    [
        (locus, f"tests/data/kp/scheme_sample/{locus}.tfa")
        for locus in ["accB_S", "accC_S", "accD_S"]
    ],
)
def test_get_locus_file(locus, file_path, scheme_path):
    locus_file = get_locus_file(locus, scheme_path)
    assert str(locus_file) == file_path


@pytest.mark.exception
def test_get_locus_file_not_found(scheme_path):
    with pytest.raises(FileNotFoundError):
        get_locus_file("loci_absent", scheme_path)


##############################################
# --- get_sequence_from_fasta_file tests --- #
##############################################


@pytest.fixture
def file_path():
    return Path(
        "tests/data/kp/scheme_sample_blastdb/scheme_sample_blastdb.fasta"
    )


def test_get_sequence_from_fasta_file(file_path):
    seq = get_sequence_from_fasta_file(file_path, "accB_S_1")
    assert seq[:15] == "ATGGATATTCGTAAG"
    assert len(seq) == 468


@pytest.mark.exception
def test_get_sequence_from_fasta_file_error(file_path):
    with pytest.raises(ContigNotFound) as cnf:
        get_sequence_from_fasta_file(file_path, "accB_S_4")
    assert str(cnf.value) == f"Contig accB_S_4 not found in {file_path}."


#################################
# --- get_num_alleles tests --- #
#################################


def test_get_num_alleles_valid():
    scheme_dir = Path("tests/data/kp/scheme_sample/")
    num_alleles = get_num_alleles(scheme_dir)
    expected_numbers = {"accB_S": 1, "accC_S": 1, "accD_S": 1}
    assert num_alleles == expected_numbers
