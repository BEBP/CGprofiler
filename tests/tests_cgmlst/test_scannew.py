import hashlib
import pytest

from pathlib import Path

from cgprofiler.cgmlst.scannew import scannew


# alleles from Kp scheme were defined as CDS, i.e., no change
@pytest.mark.parametrize("cds", [False, True])
def test_scannew_kp(kp_genome, cds):
    """Test that alleles detected considering only a sample of the database
    match the real-life alleles.
    """
    query_file = kp_genome
    no_exact_match_loci = ["accC_S", "accD_S"]
    scheme_dir = Path("tests/data/kp/scheme_sample")

    with open(Path("tests/data/test_scannew/kp_accC_S_20.tfa"), "r") as f:
        f.readline()
        accc_s_20 = f.readline().strip()

    with open(Path("tests/data/test_scannew/kp_accD_S_37.tfa"), "r") as f:
        f.readline()
        accd_s_37 = f.readline().strip()

    new_alleles_df, scannew_dict = scannew(
        query_file,
        no_exact_match_loci,
        scheme_dir,
        num_threads=4,
        cds=cds,
        verbose=False,
        detailed=False,
        min_id=90,
        min_cov_new_allele=90,
        min_cov_incomplete=70,
    )
    expected_scannew_dict = {
        # plus sense
        "accC_S": "accC_S_" + hashlib.md5(str(accc_s_20).encode()).hexdigest(),
        # minus sense
        "accD_S": "accD_S_" + hashlib.md5(str(accd_s_37).encode()).hexdigest(),
    }
    assert scannew_dict == expected_scannew_dict

    assert (
        new_alleles_df.loc[new_alleles_df["locus"] == "accC_S", "qseq"][0]
        == accc_s_20
    )
    assert (
        new_alleles_df.loc[new_alleles_df["locus"] == "accD_S", "qseq"][1]
        == accd_s_37
    )


def test_scannew_ecoli(ecoli_genome):
    """Test that alleles detected considering only a sample of the database
    match the real-life alleles.
    """
    query_file = ecoli_genome
    no_exact_match_loci = ["b3936", "b4186"]
    scheme_dir = Path("tests/data/ecoli/scheme_sample")

    with open(Path("tests/data/test_scannew/ecoli_b3936_7.fasta"), "r") as f:
        f.readline()
        b3936_7 = f.readline().strip()

    with open(Path("tests/data/test_scannew/ecoli_b4186_6.fasta"), "r") as f:
        f.readline()
        b4186_6 = f.readline().strip()

    new_alleles_df, scannew_dict = scannew(
        query_file,
        no_exact_match_loci,
        scheme_dir,
        num_threads=4,
        cds=False,
        verbose=False,
        detailed=False,
        min_id=90,
        min_cov_new_allele=90,
        min_cov_incomplete=70,
    )
    expected_scannew_dict = {
        # plus sense
        "b4186": "b4186_" + hashlib.md5(str(b4186_6).encode()).hexdigest(),
        # minus sense
        "b3936": "b3936_" + hashlib.md5(str(b3936_7).encode()).hexdigest(),
    }
    assert scannew_dict == expected_scannew_dict

    assert (
        new_alleles_df.loc[new_alleles_df["locus"] == "b4186", "qseq"][1]
        == b4186_6
    )

    assert (
        new_alleles_df.loc[new_alleles_df["locus"] == "b3936", "qseq"][0]
        == b3936_7
    )
