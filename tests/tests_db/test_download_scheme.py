import pytest

from pathlib import Path
from cgprofiler.db.download_schemes import download_scheme

scheme_file = Path("data/scheme_url.json")


def test_download_scheme_av(tmp_path, capsys):
    download_scheme(scheme_file, "av", tmp_path)
    captured = capsys.readouterr()
    assert "Klebsiella pneumoniae" in captured.out
    assert "Escherichia coli" in captured.out
