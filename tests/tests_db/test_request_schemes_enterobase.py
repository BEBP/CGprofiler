import pytest
import requests

from bs4 import BeautifulSoup

from cgprofiler.db.request_schemes_enterobase import (
    download_locus_file,
    download_scheme,
    get_last_update_locus,
)

######################################
# --- get_last_update_locus tests ---#
######################################


def get_mock_html_content() -> str:
    """
    Returns mock HTML content for testing purposes.
    """
    return """
    <a href="locus1.fasta.gz">locus1.fasta.gz</a>\t03-Feb-2024 02:18\t1635
    <a href="locus2.fasta.gz">locus2.fasta.gz</a>\t04-Feb-2024 02:19\t1636
    <a href="locus3.fasta.gz">locus3.fasta.gz</a>\t05-Feb-2024 02:20\t1637
    <a href="wrong_extension">wrong_extension</a> \t31-Feb-2024 02:21\t1638
    """


def test_get_last_update_locus():
    scheme_html_parser = BeautifulSoup(get_mock_html_content(), "html.parser")
    result = get_last_update_locus(scheme_html_parser)

    assert result == {
        "locus1": "03-Feb-2024",
        "locus2": "04-Feb-2024",
        "locus3": "05-Feb-2024",
    }


@pytest.mark.parametrize(
    "url, nb_loci",
    [
        (
            "https://enterobase.warwick.ac.uk/schemes/Escherichia.cgMLSTv1/",
            2513,
        ),
        (
            "https://enterobase.warwick.ac.uk/schemes/Salmonella.cgMLSTv2/",
            3002,
        ),
    ],
)
@pytest.mark.requests
def test_get_last_update_locus_real_url(url, nb_loci):
    r = requests.get(url)
    soup = BeautifulSoup(r.content, "html.parser")

    result = get_last_update_locus(soup)

    assert len(result) == nb_loci


####################################
# --- download_loci_files tests ---#
####################################


@pytest.mark.parametrize(
    "scheme_url, loci, min_num_sequences",
    [
        (
            "https://enterobase.warwick.ac.uk/schemes/Escherichia.cgMLSTv1/",
            ["b0163", "EAKF1_RS07940", "AEJV01_03887"],
            (808, 631, 506),
        ),
        (
            "https://enterobase.warwick.ac.uk/schemes/Salmonella.cgMLSTv2/",
            ["SPAB_04503", "STMMW_01201", "STMMW_04761"],
            (261, 183, 356),
        ),
    ],
)
@pytest.mark.requests
def test_download_loci_file(tmp_path, scheme_url, loci, min_num_sequences):
    download_dir = tmp_path / "downloads"
    download_dir.mkdir()

    for locus, expected_min_num_sequences in zip(loci, min_num_sequences):
        locus_url = f"{scheme_url}/{locus}.fasta.gz"
        download_path = download_dir / f"{locus}.fasta"
        download_locus_file(locus_url, download_path)

        assert download_path.exists()

        with open(download_path, "r") as file:
            content = file.read()
            num_sequences = content.count(">")
            assert num_sequences >= expected_min_num_sequences


################################################
# --- download_scheme_from_enterobase tests ---#
################################################


@pytest.mark.parametrize(
    "url, nb_loci",
    [
        (
            "https://enterobase.warwick.ac.uk/schemes/Escherichia.cgMLSTv1/",
            2513,
        ),
        (
            "https://enterobase.warwick.ac.uk/schemes/Salmonella.cgMLSTv2/",
            3002,
        ),
    ],
)
@pytest.mark.requests
def test_download_scheme(url, nb_loci, tmp_path):
    download_dir = tmp_path / "scheme_entire"
    download_dir.mkdir()

    download_scheme(url, download_dir)

    downloaded_files = list(download_dir.iterdir())
    assert len(downloaded_files) == nb_loci
