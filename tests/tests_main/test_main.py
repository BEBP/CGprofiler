import pytest

from cgprofiler.__main__ import main


def test_main_no_args_kp(capsys, monkeypatch):
    """Print help and exit if no arg provided."""
    args_kp = ["cgprofiler"]
    monkeypatch.setattr("sys.argv", args_kp)

    with pytest.raises(SystemExit) as excinfo:
        main()

    captured = capsys.readouterr()
    assert "cgMLST tool based on BLAST." in captured.err
    assert excinfo.value.code == 1
